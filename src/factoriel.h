#ifndef __FACTORIEL__
#define __FACTORIEL__

/** 
 * Cette classe contient une méthode pour calculer la valeur factorielle
 * d'une valeur donnée en entrée.
 * */
class Factoriel
{
public:
	int calculerFactoriel(int valeur);
};

#endif

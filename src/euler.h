#ifndef __EULER__
#define __EULER__

#include <exception>

#include "factoriel.h"

/** Cette classe fait le calcul de l'estimation de la valeur de "e".
 */
class Euler
{
private:
	Factoriel *f;

public:
	Euler();
	~Euler();
	float estimerValeur(int iterations);
};

#endif

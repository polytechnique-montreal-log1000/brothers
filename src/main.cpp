
#include <iostream>
#include <iomanip>

#include "euler.h"

/** Cette application estime la valeur de la constante Euleur (la
 * constante "e" du logarithme naturel) en utilisant la méthode
 * séquentielle de Brothers (2004). Voir : 
 * https://www.intmath.com/exponential-logarithmic-functions/calculating-e.php
 */
int main(int argc, char **argv)
{

	if (argc < 2)
	{
		std::cout << "Erreur : Besoin d'un argument indiquant le";
		std::cout << "nombre d'itérations à faire";
		std::cout << std::endl;
		return -1;
	}

	// Transformation du char* en valeur entière.
	int iterations = atoi(argv[1]);
	Euler *e = new Euler();
	float estimation = e->estimerValeur(iterations);

	std::cout << std::fixed;
	std::cout << std::setprecision(10);
	std::cout << "Valeur estimée : " << estimation << std::endl;
	std::cout << "Valeur réelle :  "
			  << "2.7182818284" << std::endl;

	delete e;

	return 0;
}


#include "factoriel.h"

int Factoriel::calculerFactoriel(int valeur)
{
	// Le calcul de la valeur factoriel devient rapidement très grand
	// et peut dépasser le maximum que peut contenir une valeur "int".
	// Si on demande une valeur >12, on retourne une erreur (-1).
	// INT_MAX est 2,1 milliards pour les compilateurs/processeurs
	// conventionnels, et 13! dépasse cette valeur.
	if (valeur > 12)
		return -1;

	// Calcul de la valeur factorielle.
	int retour = 1;
	for (int i = 1; i <= valeur; i++)
		retour = retour * i;

	return retour;
}

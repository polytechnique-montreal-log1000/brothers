
EXEC=brothers
SOURCE=src
BINAIRE=bin
TESTS=test

# ============================
# Compilation de l'exécutable
# ============================

all: $(BINAIRE)/$(EXEC)
	./$(BINAIRE)/$(EXEC) 4

$(BINAIRE)/$(EXEC): $(BINAIRE)/main.o $(BINAIRE)/factoriel.o $(BINAIRE)/euler.o
	g++ -o $@ $^

$(BINAIRE)/main.o: $(SOURCE)/main.cpp $(SOURCE)/euler.h
	mkdir -p $(BINAIRE)
	g++ -o $@ -c $<

$(BINAIRE)/factoriel.o: $(SOURCE)/factoriel.cpp $(SOURCE)/factoriel.h
	g++ -o $@ -c $<

$(BINAIRE)/euler.o: $(SOURCE)/euler.cpp $(SOURCE)/euler.h $(SOURCE)/factoriel.h
	g++ -o $@ -c $<

# =====================
# Compilation des tests
# =====================

test: $(TESTS)/$(BINAIRE)/$(EXEC)_test
	./$(TESTS)/$(BINAIRE)/$(EXEC)_test

$(TESTS)/$(BINAIRE)/$(EXEC)_test: $(TESTS)/$(BINAIRE)/main.o $(TESTS)/$(BINAIRE)/factoriel_test.o $(BINAIRE)/factoriel.o $(TESTS)/$(BINAIRE)/eulerfactoriel_test.o $(BINAIRE)/euler.o
	g++ -o $@ $^ -lcppunit

$(TESTS)/$(BINAIRE)/main.o: $(TESTS)/$(SOURCE)/main.cpp $(TESTS)/$(SOURCE)/factoriel_test.h $(TESTS)/$(SOURCE)/eulerfactoriel_test.h
	mkdir -p $(TESTS)/$(BINAIRE)
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/eulerfactoriel_test.o: $(TESTS)/$(SOURCE)/eulerfactoriel_test.cpp $(TESTS)/$(SOURCE)/eulerfactoriel_test.h $(SOURCE)/euler.h
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/factoriel_test.o: $(TESTS)/$(SOURCE)/factoriel_test.cpp $(TESTS)/$(SOURCE)/factoriel_test.h $(SOURCE)/factoriel.h
	g++ -o $@ -c $<

# ===========
# Utilitaires
# ===========

clean:
	rm -rf $(BINAIRE)/*.o
	rm -rf $(BINAIRE)/$(EXEC)
	rm -rf $(TESTS)/$(BINAIRE)/*.o
	rm -rf $(TESTS)/$(BINAIRE)/$(EXEC)_test



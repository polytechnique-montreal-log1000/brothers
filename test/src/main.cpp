
// Librairies CppUnit nécessaires
#include <cppunit/ui/text/TestRunner.h>

// Fichiers contenant les tests
#include "factoriel_test.h"
#include "eulerfactoriel_test.h"

/**
 * Tests pour l'application "brothers"
 */
int main(int argc, char** argv) {

    // Objet pour exécuter les tests et présenter sous forme textuelle.
    CppUnit::TextUi::TestRunner runner;

    // Ajout des tests unitaires de la classe Factoriel
    runner.addTest(FactorielTest::suite());

    // Ajout des tests d'intégration Euler<->Factoriel
    runner.addTest(EulerFactorielTest::suite());

    // Exécute les tests et affiche les résultats.
    runner.run();

    return 0;
}


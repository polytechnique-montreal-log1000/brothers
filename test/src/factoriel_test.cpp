
#include "factoriel_test.h"

void FactorielTest::setUp() {
		this->objet_a_tester = new Factoriel();
}

void FactorielTest::tearDown() {
		delete this->objet_a_tester;
}

// Correspond à d1 = <{0}, {1}>
void FactorielTest::test_calculerFactoriel() {
	int valeur = this->objet_a_tester->calculerFactoriel(0);
	CPPUNIT_ASSERT_EQUAL(1, valeur);
}

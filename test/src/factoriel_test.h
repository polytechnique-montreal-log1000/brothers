// Classe qui teste la classe Factoriel
// Avec le framework CppUnit

// Librairies CppUnit nécessaires.
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Le fichier à tester, qui se trouve dans un répertoire différent.
#include "../../src/factoriel.h"

class FactorielTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(FactorielTest);
    CPPUNIT_TEST(test_calculerFactoriel);
    CPPUNIT_TEST_SUITE_END();
    
private:
	Factoriel* objet_a_tester;
    
public:
	// Fonctions d'échafaudage
    void setUp();
    void tearDown();
    
    // Fonctions de tests
    void test_calculerFactoriel();
};
